let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}

function enqueue(item){
    collection.push(item)
    return collection
}


function dequeue(){
    collection.shift()
    return collection
}

function front(){
   let firstvalue = collection[0]
   return firstvalue
}
    
function size(){
    let arrLength = collection.length
    return arrLength
}

function isEmpty(){
    let arrLength = collection.length
    let isEmpty
    if(arrLength > 0) isEmpty = false

    return isEmpty
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};